import requests
import json

url_main = 'http://10.201.23.51:11000/'

serviceId = {"AMIL-chatbot": "AIMLService/rest/check/post",
             "Intent-Miner": "intentmining",
             "Negation-detector": "negations/services/answerextractor"}

serviceInputKey = {"AMIL-chatbot": "text",
                   "Intent-Miner": "val",
                   "Negation-detector": "questions"}

serviceHeader = {"content-type": "application/json",
                 "apikey": "f9c751467a874c80a6d4b88926959227"}


## sending the queries as JSON object to the Wipro Holmes APIs and getting the response
def invokeAPI(service, userText):
    url = url_main + serviceId[service]
    userText = {serviceInputKey[service]: userText}
    jsonObjIn = json.dumps(userText)

    # print "before api call"

    # print url
    # print userText
    # print serviceHeader

    # get response
    response = requests.post(url_main + serviceId[service], data=jsonObjIn, headers=serviceHeader)

    # print "response"
    # print response.ok
    # print response

    if response.ok == True:
        response_data = json.loads(response.content);
        for resp in response_data:
            print
            "Agent: " + response_data[resp]
    return response.ok


print
"Agent: Hi, Welcome!! How can I help you :"

customer_input = ""

customer_input = raw_input("Customer: ")

while (customer_input.upper() != "NO"):

    if invokeAPI("AMIL-chatbot", customer_input) == True:
        pass
    elif invokeAPI("Intent-Miner", customer_input) == True:
        pass
    elif invokeAPI("Negation-detector", customer_input) == True:
        pass
    else:
        print
        "Agent: sorry unable to answer"

    print
    "Agent: Do you have any other queries (Yes / No)?"
    customer_input = raw_input("Customer: ")
print "Happy to help!! Have a nice day!!!"